import model.Network;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    private static final Logger logger = Logger.getLogger("App");

    public static void main(String[] args) {
        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        logger.log(Level.INFO,  " correct of {0}",results.correct );
                logger.log(Level.INFO, " = {0}",results.trials);
                logger.log(Level.INFO,"%{0}",results.percentage * 100);
    }
}
